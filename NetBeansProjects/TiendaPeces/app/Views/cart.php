<!doctype html>
<html lang="en">

    <!-- Head -->
    <head>
        <!-- Page Meta Tags-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">

        <!-- Custom Google Fonts-->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500;600&family=Roboto:wght@300;400;700&display=auto"
              rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="./assets/images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="./assets/images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="./assets/images/favicon/favicon-16x16.png">
        <link rel="mask-icon" href="./assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="./assets/css/libs.bundle.css" />

        <!-- Main CSS -->
        <link rel="stylesheet" href="./assets/css/theme.bundle.css" />

        <!-- Fix for custom scrollbar if JS is disabled-->
        <noscript>
        <style>
            /**
                * Reinstate scrolling for non-JS clients
                */
            .simplebar-content-wrapper {
                overflow: auto;
            }
        </style>
        </noscript>

        <!-- Page Title -->
        <title>Pececillos Ruta 12 | Carrito</title>

    </head>
    <body class="">

        <!-- Main Section-->
        <section class="mt-0 overflow-lg-hidden  vh-lg-100">
            <!-- Page Content Goes Here -->
            <div class="container">
                <div class="row g-0 vh-lg-100">
                    <div class="col-12 col-lg-7 pt-5 pt-lg-10">
                        <div class="pe-lg-5">
                            <!-- Logo-->
                            <a class="navbar-brand fw-bold fs-3 flex-shrink-0 mx-0 px-0" href="home">
                                <div class="d-flex align-items-center">
                                    <svg class="f-w-7" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 77.53 72.26"><path d="M10.43,54.2h0L0,36.13,10.43,18.06,20.86,0H41.72L10.43,54.2Zm67.1-7.83L73,54.2,68.49,62,45,48.47,31.29,72.26H20.86l-5.22-9L52.15,0H62.58l5.21,9L54.06,32.82,77.53,46.37Z" fill="currentColor" fill-rule="evenodd"/></svg>
                                </div>
                            </a>
                            <!-- / Logo-->
                            <nav class="d-none d-md-block">
                                <ul class="list-unstyled d-flex justify-content-start mt-4 align-items-center fw-bolder small">
                                    <li class="me-4"><a class="nav-link-checkout active"
                                                        href="cart">Tu carrito</a></li>
                                    <li class="me-4"><a class="nav-link-checkout "
                                                        href="checkout">Información</a></li>
                                    <li class="me-4"><a class="nav-link-checkout "
                                                        href="checkout-shipping">Método de envío</a></li>
                                    <li><a class="nav-link-checkout nav-link-last "
                                           href="checkout-payment">Forma de pago</a></li>
                                </ul>
                            </nav>                        <div class="mt-5">
                                <h3 class="fs-5 fw-bolder mb-0 border-bottom pb-4">Tu carrito</h3>
                                <div class="table-responsive">
                                    <table class="table align-middle">
                                        <tbody>
                                            <?php foreach ($nuevoPedido as $subArray): ?>

                                                <tr>
                                                    <th class="p-4 align-middle" scope="row">
                                                        <div class="d-flex align-items-center"><img class="img-fluid" src="<?= $subArray['imagen'] ?>" alt="..." width="80">
                                                            <div class="ms-3">
                                                                <p class="text-uppercase h4 mb-0"><?= $subArray['nombre'] ?></p>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <td class="p-4 align-middle">
                                                        <h3 class="h5 fw-normal mb-0"><?= $subArray['precio'] ?></h3>
                                                    </td>
                                                    <td class="p-4 align-middle text-end">
                                                        <button class="h5 fw-normal mb-0" onclick="alerta()" style="background-color: white; border: none;"><a href="/eliminar/<?= $subArray['id'] ?>" class="bi-trash me-1 text-black"></a> </button>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5 bg-light pt-lg-10 aside-checkout pb-5 pb-lg-0 my-5 my-lg-0">
                        <div class="p-4 py-lg-0 pe-lg-0 ps-lg-5">
                            <div class="pb-4 border-bottom">
                                <div class="d-flex flex-column flex-md-row justify-content-md-between mb-4 mb-md-2">
                                    <div>
                                        <p class="m-0 fw-bold fs-5"></p>
                                        <span class="text-muted small"></span>
                                    </div>
                                    <p class="m-0 fs-5 fw-bold"></p>
                                </div>
                            </div>
                            <div class="py-4">
                                <div class="input-group mb-0">
                                    <input type="text" class="form-control" placeholder="Indica un código de descuento">
                                    <button class="btn btn-secondary btn-sm px-4">Aplicar</button>
                                </div>
                            </div>
                            <a href="checkout" class="btn btn-dark w-100 text-center" role="button">Iniciar compra</a>                    </div>
                    </div>
                </div>
            </div>
            <!-- /Page Content -->
        </section>
        <!-- / Main Section-->

        <!-- Theme JS -->
        <!-- Vendor JS -->
        <script src="./assets/js/vendor.bundle.js"></script>

        <!-- Theme JS -->
        <script src="./assets/js/theme.bundle.js"></script>
    </body>

</html>