<!doctype html>
<html lang="en">

    <!-- Head -->
    <head>
        <!-- Page Meta Tags-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">

        <!-- Custom Google Fonts-->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500;600&family=Roboto:wght@300;400;700&display=auto"
              rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="./assets/images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="./assets/images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="./assets/images/favicon/favicon-16x16.png">
        <link rel="mask-icon" href="./assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="/assets/css/libs.bundle.css" />

        <!-- Main CSS -->
        <link rel="stylesheet" href="/assets/css/theme.bundle.css" />

        <!-- Fix for custom scrollbar if JS is disabled-->
        <noscript>
        <style>
            /**
                * Reinstate scrolling for non-JS clients
                */
            .simplebar-content-wrapper {
                overflow: auto;
            }
        </style>
        </noscript>

        <!-- Page Title -->
        <title>Pececillos Ruta 12 | Producto</title>

    </head>
    <body class="">

        <!-- Navbar -->
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light bg-white flex-column border-0  ">
            <div class="container-fluid">
                <div class="w-100">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">

                        <!-- Logo-->
                        <a class="navbar-brand fw-bold fs-3 m-0 p-0 flex-shrink-0 order-0" href="/home">
                            <div class="d-flex align-items-center">
                                <img class="img-fluid w-50 h-50" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4-njHQj78E27NGwwNNpyNtYDlFYSMP8Ng9w&usqp=CAU" alt="HTML Bootstrap Template by Pixel Rocket">
                            </div>
                        </a>
                        <!-- / Logo-->

                        <!-- Navbar Icons-->
                        <ul class="list-unstyled mb-0 d-flex align-items-center order-1 order-lg-2 nav-sidelinks">

                            <!-- Mobile Nav Toggler-->
                            <li class="d-lg-none">
                                <span class="nav-link text-body d-flex align-items-center cursor-pointer" data-bs-toggle="collapse"
                                      data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                                      aria-label="Toggle navigation"><i class="ri-menu-line ri-lg me-1"></i> Menú</span>
                            </li>
                            <!-- /Mobile Nav Toggler-->

                            <!-- Navbar Search-->
                            <li class="d-none d-sm-block">


                                <!-- Search navbar overlay-->
                                <div class="navbar-search d-none">
                                    <div class="input-group mb-3 h-100">
                                        <span class="input-group-text px-4 bg-transparent border-0"><i
                                                class="ri-search-line ri-lg"></i></span>
                                        <input type="text" class="form-control text-body bg-transparent border-0"
                                               placeholder="Enter your search terms...">
                                        <span
                                            class="input-group-text px-4 cursor-pointer disable-child-pointer close-search bg-transparent border-0"><i
                                                class="ri-close-circle-line ri-lg"></i></span>
                                    </div>
                                </div>
                                <div class="search-overlay"></div>
                                <!-- / Search navbar overlay-->

                            </li>
                            <!-- /Navbar Search-->

                            <!-- Navbar Login-->
                            <li class="ms-1 d-none d-lg-inline-block">
                                <a class="nav-link text-body" href="login">
                                    Cuenta
                                </a>
                            </li>
                            <!-- /Navbar Login-->

                            <!-- Navbar Cart Icon-->
                            <li class="ms-1 d-inline-block position-relative dropdown-cart">
                                <button class="nav-link me-0 disable-child-pointer border-0 p-0 bg-transparent text-body"
                                        type="button">
                                    Carro
                                </button>
                                <div class="cart-dropdown dropdown-menu">

                                    <!-- Cart Header-->
                                    <div class="d-flex justify-content-between align-items-center border-bottom pt-3 pb-4">
                                        <h6 class="fw-bolder m-0">Carrito</h6>
                                        <i class="ri-close-circle-line text-muted ri-lg cursor-pointer btn-close-cart"></i>
                                    </div>
                                    <!-- / Cart Header-->

                                    <!-- Cart Items-->
                                    <div>

                                        <!-- Cart Product-->

                                        <!-- /Cart Items-->

                                        <!-- Cart Summary-->
                                        <div>
                                            <div class="pt-3">
                                                <div class="d-flex flex-column flex-md-row justify-content-md-between align-items-md-start mb-4 mb-md-2">
                                                    <div>
                                                        <p class="m-0 fw-bold fs-5"></p>
                                                        <span class="text-muted small"></span>
                                                    </div>
                                                    <p class="m-0 fs-5 fw-bold"></p>
                                                </div>
                                            </div>
                                            <a href="cart" class="btn btn-outline-dark w-100 text-center mt-4" role="button">Ver Carrito</a>
                                            <a href="checkout" class="btn btn-dark w-100 text-center mt-2" role="button">Comprar</a>
                                        </div>
                                        <!-- / Cart Summary-->
                                    </div>


                            </li>
                            <!-- /Navbar Cart Icon-->

                        </ul>
                        <!-- Navbar Icons-->                

                        <!-- Main Navigation-->
                        <div class="flex-shrink-0 collapse navbar-collapse navbar-collapse-light w-auto flex-grow-1 order-2 order-lg-1"
                             id="navbarNavDropdown">

                            <!-- Menu-->
                            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">

                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="/aguaDulce" role="button"  aria-expanded="false">
                                        Agua dulce
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="/aguaSalada" role="button"  aria-expanded="false">
                                        Agua salada
                                    </a>
                                </li>


                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Sesión
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="register">Registrarse</a></li>
                                        <li><a class="dropdown-item" href="forgotten-password">Contraseña olvidada</a></li>
                                    </ul>
                                </li>
                            </ul>                    <!-- / Menu-->

                        </div>
                        <!-- / Main Navigation-->

                    </div>
                </div>
            </div>
        </nav>
        <!-- / Navbar-->    <!-- / Navbar-->

        <!-- Main Section-->
        <section class="mt-0 ">
            <!-- Page Content Goes Here -->            

            <!-- Breadcrumbs-->
            <div class="bg-dark py-6">
                <div class="container-fluid">
                    <nav class="m-0" aria-label="breadcrumb">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item breadcrumb-light"><a href="/home">Inicio</a></li>
                            <li class="breadcrumb-item breadcrumb-light"><a href="/aguaDulce">Agua dulce</a></li>
                            <li class="breadcrumb-item  breadcrumb-light active" aria-current="page">
                                <?= $producto->nombre ?>
                            </li>
                        </ol>
                    </nav>            </div>
            </div>
            <!-- / Breadcrumbs-->

            <div class="container-fluid mt-5">

                <!-- Product Top Section-->
                <div class="row g-9" data-sticky-container>

                    <!-- Product Images-->
                    <div class="col-12 col-md-6 col-xl-7">
                        <div class="row g-3" data-aos="fade-right">
                            <div class="col-12">
                                <picture>
                                    <img class="img-fluid w-100" data-zoomable src="data:image/jpg;base64,<?= base64_encode($producto->imagen) ?>" alt="...">
                                </picture>
                            </div>

                        </div>
                    </div>
                    <!-- /Product Images-->

                    <!-- Product Information-->
                    <div class="col-12 col-md-6 col-lg-5">
                        <form action="/anyadirProd" method="post" class="row gx-4 gx-lg-5 align-items-center">
                            <div class="sticky-top top-5">
                                <div class="pb-3" data-aos="fade-in">
                                    <input class="mb-1 fs-2 fw-bold" id="nombreProd" value="<?= $producto->nombre ?>" readonly name="nombre" style="border: none; outline: none;"/>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <input class="fs-4 m-0" value="$<?= $producto->precio ?>" name="precio" readonly style="border: none; outline: none;"/>
                                    </div>
                                    <div class="border-top mt-4 mb-3 product-option">
                                    </div>

                                    <input id="id" readonly name="id" class="d-none" style="display: none;" value="<?= $producto->id ?>"/>
                                    <input id="imagen" readonly name="imagen" class="d-none" style="display: none;" value="data:image/jpg;base64,<?= base64_encode($producto->imagen) ?>"/>


                                    <button class="btn btn-dark w-100 mt-4 mb-0 hover-lift-sm hover-boxshadow">Añadir al carrito</button>

                                    <!-- Product Accordion -->
                                    <div class="accordion" id="accordionProduct">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingOne">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Detalles del pez
                                                </button>
                                            </h2>
                                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionProduct">
                                                <div class="accordion-body">
                                                    <p class="m-0"><?= $producto->descripcion ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- / Product Accordion-->
                                </div>                    
                            </div>
                        </form>
                    </div>
                    <!-- / Product Information-->
                </div>
                <!-- / Product Top Section-->



                <!-- /Page Content -->
        </section>
        <!-- / Main Section-->

        <!-- Footer -->
        <footer class="border-top py-5 mt-4  ">
            <div class="container-fluid">
                <div class="d-flex justify-content-between align-items-center flex-column flex-lg-row">
                    <div>
                        <ul class="list-unstyled">
                            <li class="d-inline-block me-1"><a class="text-decoration-none text-dark-hover transition-all"
                                                               href="#"><i class="ri-instagram-fill"></i></a></li>
                            <li class="d-inline-block me-1"><a class="text-decoration-none text-dark-hover transition-all"
                                                               href="#"><i class="ri-facebook-fill"></i></a></li>
                            <li class="d-inline-block me-1"><a class="text-decoration-none text-dark-hover transition-all"
                                                               href="#"><i class="ri-twitter-fill"></i></a></li>
                            <li class="d-inline-block me-1"><a class="text-decoration-none text-dark-hover transition-all"
                                                               href="#"><i class="ri-snapchat-fill"></i></a></li>
                        </ul>
                    </div>
                    <div class="d-flex align-items-center justify-content-end flex-column flex-lg-row">
                        <p class="small m-0 text-center text-lg-start">&copy; PESCADORES S.A</p>
                        <ul class="list-unstyled mb-0 ms-lg-4 mt-3 mt-lg-0 d-flex justify-content-end align-items-center">
                            <li class="bg-light p-2 d-flex align-items-center justify-content-center me-2">
                                <i class="pi pi-sm pi-paypal"></i></li>
                            <li class="bg-light p-2 d-flex align-items-center justify-content-center me-2">
                                <i class="pi pi-sm pi-mastercard"></i></li>
                            <li class="bg-light p-2 d-flex align-items-center justify-content-center me-2">
                                <i class="pi pi-sm pi-american-express"></i></li>
                            <li class="bg-light p-2 d-flex align-items-center justify-content-center"><i
                                    class="pi pi-sm pi-visa"></i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>    <!-- / Footer-->  

        <!-- Theme JS -->
        <!-- Vendor JS -->
        <script src="/assets/js/vendor.bundle.js"></script>

        <!-- Theme JS -->
        <script src="/assets/js/theme.bundle.js"></script>
    </body>

</html>
