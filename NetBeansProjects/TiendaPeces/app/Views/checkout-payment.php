<!doctype html>
<html lang="en">

<!-- Head -->
<head>
  <!-- Page Meta Tags-->
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="keywords" content="">

  <!-- Custom Google Fonts-->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500;600&family=Roboto:wght@300;400;700&display=auto"
    rel="stylesheet">

  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="./assets/images/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="./assets/images/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="./assets/images/favicon/favicon-16x16.png">
  <link rel="mask-icon" href="./assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <!-- Vendor CSS -->
  <link rel="stylesheet" href="./assets/css/libs.bundle.css" />

  <!-- Main CSS -->
  <link rel="stylesheet" href="./assets/css/theme.bundle.css" />

  <!-- Fix for custom scrollbar if JS is disabled-->
  <noscript>
    <style>
      /**
          * Reinstate scrolling for non-JS clients
          */
      .simplebar-content-wrapper {
        overflow: auto;
      }
    </style>
  </noscript>

  <!-- Page Title -->
  <title>Pececillos Ruta 12 | Forma de pago</title>

</head>
<body class="">

    <!-- Main Section-->
    <section class="mt-0  vh-lg-100">
        <!-- Page Content Goes Here -->
        <div class="container">
            <div class="row g-0 vh-lg-100">
                <div class="col-lg-7 pt-5 pt-lg-10">
                    <div class="pe-lg-5">
                        <!-- Logo-->
                        <a class="navbar-brand fw-bold fs-3 flex-shrink-0 mx-0 px-0" href="home">
                                <div class="d-flex align-items-center">
                                    <svg class="f-w-7" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 77.53 72.26"><path d="M10.43,54.2h0L0,36.13,10.43,18.06,20.86,0H41.72L10.43,54.2Zm67.1-7.83L73,54.2,68.49,62,45,48.47,31.29,72.26H20.86l-5.22-9L52.15,0H62.58l5.21,9L54.06,32.82,77.53,46.37Z" fill="currentColor" fill-rule="evenodd"/></svg>
                                </div>
                            </a>
                        <!-- / Logo-->
                        <nav class="d-none d-md-block">
                            <ul class="list-unstyled d-flex justify-content-start mt-4 align-items-center fw-bolder small">
                                <li class="me-4"><a class="nav-link-checkout "
                                        href="cart">Tu carrito</a></li>
                                <li class="me-4"><a class="nav-link-checkout "
                                        href="checkout">Información</a></li>
                                <li class="me-4"><a class="nav-link-checkout "
                                        href="checkout-shipping">Método de envío</a></li>
                                <li><a class="nav-link-checkout nav-link-last active"
                                        href="checkout-payment">Forma de pago</a></li>
                            </ul>
                        </nav>                        <div class="mt-5">
                            <!-- Checkout Information Summary -->
                            <ul class="list-group mb-5 d-none d-lg-block rounded-0">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    <div class="d-flex justify-content-start align-items-center">
                                        <span class="text-muted small me-2 f-w-36 fw-bolder">Correo de contacto</span>
                                        <span class="small">test@email.com</span>
                                    </div>
                                    <a href="checkout" class="text-muted small" role="button">Cambiar</a>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    <div class="d-flex justify-content-start align-items-center">
                                        <span class="text-muted small me-2 f-w-36 fw-bolder">Dirección de envío</span>
                                        <span class="small">123 Street, London, SM3 5TY, United Kingdom</span>
                                    </div>
                                    <a href="checkout-shipping" class="text-muted small" role="button">Cambiar</a>
                                </li>
                              
                            </ul><!-- / Checkout Information Summary-->
                            
                            <!-- Checkout Panel Information-->
                            <h3 class="fs-5 fw-bolder mb-4 border-bottom pb-4">Información de pago</h3>
                            
                            <div class="row">
                            
                              <!-- Payment Option-->
                              <div class="col-12">
                                <div class="form-check form-group form-check-custom form-radio-custom mb-3">
                                  <input class="form-check-input" type="radio" name="checkoutPaymentMethod" id="checoutPaymentStripe" checked>
                                  <label class="form-check-label" for="checoutPaymentStripe">
                                    <span class="d-flex justify-content-between align-items-start">
                                      <span>
                                        <span class="mb-0 fw-bolder d-block">Tarjeta de crédito</span>
                                      </span>
                                      <i class="ri-bank-card-line"></i>
                                    </span>
                                  </label>
                                </div>
                              </div>
                            
                              <!-- Payment Option-->
                              <div class="col-12">
                                <div class="form-check form-group form-check-custom form-radio-custom mb-3">
                                  <input class="form-check-input" type="radio" name="checkoutPaymentMethod" id="checkoutPaymentPaypal">
                                  <label class="form-check-label" for="checkoutPaymentPaypal">
                                    <span class="d-flex justify-content-between align-items-start">
                                      <span>
                                        <span class="mb-0 fw-bolder d-block">PayPal</span>
                                      </span>
                                      <i class="ri-paypal-line"></i>
                                    </span>
                                  </label>
                                </div>
                              </div>
                            
                            </div>
                            
                            <!-- Payment Details-->
                            <div class="card-details">
                              <div class="row pt-3">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="cc-name" class="form-label">Propietario de la tarjeta de crédito</label>
                                    <input type="text" class="form-control" id="cc-name" placeholder="" required="">
                                    <small class="text-muted">Indica el nombre y apellidos completo</small>
                                  </div>
                                </div>
                            
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="cc-number" class="form-label">Número de la tarjeta de crédito</label>
                                    <input type="text" class="form-control" id="cc-number" placeholder="" required="">
                                  </div>
                                </div>
                            
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="cc-expiration" class="form-label">Fecha de expiración</label>
                                    <input type="text" class="form-control" id="cc-expiration" placeholder="" required="">
                                  </div>
                                </div>
                            
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="d-flex">
                                      <label for="cc-cvv" class="form-label d-flex w-100 justify-content-between align-items-center">Código de seguridad</label>
                                      <button type="button" class="btn btn-link p-0 fw-bolder fs-xs text-nowrap" data-bs-toggle="tooltip"
                                              data-bs-placement="top"
                                              title="El CVV es el número de tu tarjeta de crédito o débito que se encuentra en la parte posterior de la tarjeta.">
                                        ¿Qué es?
                                      </button>
                                    </div>
                                    <input type="text" class="form-control" id="cc-cvv" placeholder="" required="">
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- / Payment Details-->
                            
                            <!-- Paypal Info-->
                            <div class="paypal-details bg-light p-4 d-none my-3 fw-bolder">
                              Por favor, haz clic en Completar perdido. Serás transferido a Paypal para acceder a los detalles del pago del pedido.
                            </div>
                            <!-- /Paypal Info-->
                            
                            <!-- Accept Terms Checkbox-->
                            <div class="form-group form-check m-0">
                              <input type="checkbox" class="form-check-input" id="accept-terms" checked>
                              <label class="form-check-label fw-bolder" for="accept-terms">Estoy de acuerdo con los <a href="#">términos y condiciones</a> de Pececillos Ruta 12</label>
                            </div>
                            
                            <div class="pt-5 mt-5 pb-5 border-top d-flex flex-column flex-md-row justify-content-between align-items-center">
                              <a href="checkout-shipping" class="btn ps-md-0 btn-link fw-bolder w-100 w-md-auto mb-2 mb-md-0" role="button">Volver a Método de envío</a>
                              <a href="#" class="btn btn-dark w-100 w-md-auto" role="button">Complete Pedido</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-5 bg-light pt-lg-10 aside-checkout pb-5 pb-lg-0 my-5 my-lg-0">
                    <div class="p-4 py-lg-0 pe-lg-0 ps-lg-5">
                        <div class="pb-3">
                            <!-- Cart Item-->
                           
                            </div>    <!-- / Cart Item-->
                        </div>
                        <div class="py-4 border-bottom">
                            <div class="d-flex justify-content-between align-items-center mb-2">
                                <p class="m-0 fw-bolder fs-6"></p>
                                <p class="m-0 fs-6 fw-bolder"></p>
                            </div>
                            <div class="d-flex justify-content-between align-items-center ">
                                <p class="m-0 fw-bolder fs-6"></p>
                                <p class="m-0 fs-6 fw-bolder"></p>
                            </div>
                        </div>
                        <div class="py-4 border-bottom">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <p class="m-0 fw-bold fs-5">Total</p>
                                    <span class="text-muted small"></span>
                                </div>
                                <p class="m-0 fs-5 fw-bold"></p>
                            </div>
                        </div>
                        <div class="py-4">
                            <div class="input-group mb-0">
                                <input type="text" class="form-control" placeholder="Indica un código de descuento">
                                <button class="btn btn-dark btn-sm px-4">Aplicar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Page Content -->
    </section>
    <!-- / Main Section-->


    <!-- Theme JS -->
    <!-- Vendor JS -->
    <script src="./assets/js/vendor.bundle.js"></script>
    
    <!-- Theme JS -->
    <script src="./assets/js/theme.bundle.js"></script>
</body>

</html>