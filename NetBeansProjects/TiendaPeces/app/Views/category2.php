<!doctype html>
<html lang="en">

    <!-- Head -->
    <head>
        <!-- Page Meta Tags-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">

        <!-- Custom Google Fonts-->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500;600&family=Roboto:wght@300;400;700&display=auto"
              rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="./assets/images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="./assets/images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="./assets/images/favicon/favicon-16x16.png">
        <link rel="mask-icon" href="./assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="./assets/css/libs.bundle.css" />

        <!-- Main CSS -->
        <link rel="stylesheet" href="./assets/css/theme.bundle.css" />

        <!-- Fix for custom scrollbar if JS is disabled-->
        <noscript>
        <style>
            /**
                * Reinstate scrolling for non-JS clients
                */
            .simplebar-content-wrapper {
                overflow: auto;
            }
        </style>
        </noscript>

        <!-- Page Title -->
        <title>Pececillos Ruta 12 | Peces agua salada</title>

    </head>
    <body class="">

        <!-- Navbar -->
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light bg-white flex-column border-0  ">
            <div class="container-fluid">
                <div class="w-100">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">

                        <!-- Logo-->
                        <a class="navbar-brand fw-bold fs-3 m-0 p-0 flex-shrink-0 order-0" href="home">
                            <div class="d-flex align-items-center">
                                <img class="img-fluid w-50 h-50" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4-njHQj78E27NGwwNNpyNtYDlFYSMP8Ng9w&usqp=CAU" alt="HTML Bootstrap Template by Pixel Rocket">
                            </div>
                        </a>
                        <!-- / Logo-->

                        <!-- Navbar Icons-->
                        <ul class="list-unstyled mb-0 d-flex align-items-center order-1 order-lg-2 nav-sidelinks">

                            <!-- Mobile Nav Toggler-->
                            <li class="d-lg-none">
                                <span class="nav-link text-body d-flex align-items-center cursor-pointer" data-bs-toggle="collapse"
                                      data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                                      aria-label="Toggle navigation"><i class="ri-menu-line ri-lg me-1"></i> Menú</span>
                            </li>
                            <!-- /Mobile Nav Toggler-->

                            <!-- Navbar Search-->
                            <li class="d-none d-sm-block">
                                

                                <!-- Search navbar overlay-->
                                <div class="navbar-search d-none">
                                    <div class="input-group mb-3 h-100">
                                        <span class="input-group-text px-4 bg-transparent border-0"><i
                                                class="ri-search-line ri-lg"></i></span>
                                        <input type="text" class="form-control text-body bg-transparent border-0"
                                               placeholder="Enter your search terms...">
                                        <span
                                            class="input-group-text px-4 cursor-pointer disable-child-pointer close-search bg-transparent border-0"><i
                                                class="ri-close-circle-line ri-lg"></i></span>
                                    </div>
                                </div>
                                <div class="search-overlay"></div>
                                <!-- / Search navbar overlay-->

                            </li>
                            <!-- /Navbar Search-->

                            <!-- Navbar Login-->
                            <li class="ms-1 d-none d-lg-inline-block">
                                <a class="nav-link text-body" href="login">
                                    Cuenta
                                </a>
                            </li>
                            <!-- /Navbar Login-->

                            <!-- Navbar Cart Icon-->
                            <li class="ms-1 d-inline-block position-relative dropdown-cart">
                                <button class="nav-link me-0 disable-child-pointer border-0 p-0 bg-transparent text-body"
                                        type="button">
                                    Carro
                                </button>
                                <div class="cart-dropdown dropdown-menu">

                                    <!-- Cart Header-->
                                    <div class="d-flex justify-content-between align-items-center border-bottom pt-3 pb-4">
                                        <h6 class="fw-bolder m-0">Carrito</h6>
                                        <i class="ri-close-circle-line text-muted ri-lg cursor-pointer btn-close-cart"></i>
                                    </div>
                                    <!-- / Cart Header-->

                                    <!-- Cart Items-->
                                    <div>

                                        <!-- Cart Product-->
                                        
                                    <!-- /Cart Items-->

                                    <!-- Cart Summary-->
                                    <div>
                                        <div class="pt-3">
                                            <div class="d-flex flex-column flex-md-row justify-content-md-between align-items-md-start mb-4 mb-md-2">
                                                <div>
                                                    <p class="m-0 fw-bold fs-5"></p>
                                                    <span class="text-muted small"></span>
                                                </div>
                                                <p class="m-0 fs-5 fw-bold"></p>
                                            </div>
                                        </div>
                                        <a href="cart" class="btn btn-outline-dark w-100 text-center mt-4" role="button">Ver Carrito</a>
                                        <a href="checkout" class="btn btn-dark w-100 text-center mt-2" role="button">Comprar</a>
                                    </div>
                                    <!-- / Cart Summary-->
                                </div>


                            </li>
                            <!-- /Navbar Cart Icon-->

                        </ul>
                        <!-- Navbar Icons-->                

                        <!-- Main Navigation-->
                        <div class="flex-shrink-0 collapse navbar-collapse navbar-collapse-light w-auto flex-grow-1 order-2 order-lg-1"
                             id="navbarNavDropdown">

                            <!-- Menu-->
                            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">

                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="/aguaDulce" role="button"  aria-expanded="false">
                                        Agua dulce
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="/aguaSalada" role="button"  aria-expanded="false">
                                        Agua salada
                                    </a>
                                </li>


                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Sesión
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="register">Registrarse</a></li>
                                        <li><a class="dropdown-item" href="forgotten-password">Contraseña olvidada</a></li>
                                    </ul>
                                </li>
                            </ul>                    <!-- / Menu-->

                        </div>
                        <!-- / Main Navigation-->

                    </div>
                </div>
            </div>
        </nav>
        <!-- / Navbar-->    <!-- / Navbar-->

        <!-- Main Section-->
        <section class="mt-0 ">
            <!-- Page Content Goes Here -->

            <!-- Category Top Banner -->
            <div class="py-10 bg-img-cover bg-overlay-dark position-relative overflow-hidden bg-pos-center-center rounded-0"
                 style="background-image: url(https://live.hsmob.io/storage/images/wakyma.com/wakyma.com_blog_wp-co_como-son-los-peces-de-agua-salada-y-que-necesitan-1024x576.jpg);">
                <div class="container-fluid position-relative z-index-20" data-aos="fade-right" data-aos-delay="300">
                    <h1 class="fw-bold display-6 mb-4 text-white">Peces de agua salada</h1>
                    <div class="col-12 col-md-6">
                        <p class="text-white mb-0 fs-5">
                            Los peces de agua salada, también llamados peces marinos o peces de mar, son peces que viven en el agua de mar. Los peces de agua salada pueden nadar y vivir solos o en un grupo grande llamado escuela. Los peces de agua salada se mantienen muy comúnmente en acuarios para el entretenimiento.
                        </p>
                    </div>
                </div>
            </div>
            <!-- Category Top Banner -->

            <div class="container-fluid" data-aos="fade-in">
                <!-- Category Toolbar-->
                <div class="d-flex justify-content-between items-center pt-5 pb-4 flex-column flex-lg-row">
                    <div>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home">Home</a></li>
                            </ol>
                        </nav>        <h1 class="fw-bold fs-3 mb-2">Peces de agua salada</h1>
                        
                    </div>
                    
                </div>            <!-- /Category Toolbar-->

                <!-- Products-->
                <div class="row g-4 d-flex justify-content-between">
                    <?php foreach ($asalada as $asaladito): ?>
                        <div class="col-12 col-sm-6 col-lg-4 flex-fill">
                            <!-- Card Product-->
                            <div class="card border border-transparent position-relative overflow-hidden h-100 transparent">
                                <div class="card-img position-relative">
                                    <div class="card-badges">
                                        <span class="badge badge-card"><span class="f-w-2 f-h-2 bg-success rounded-circle d-block me-1"></span> Available</span>
                                    </div>
                                    <span class="position-absolute top-0 end-0 p-2 z-index-20 text-muted"><i class="ri-heart-line"></i></span>
                                    <picture class="position-relative overflow-hidden d-block bg-light">
                                        <img class="w-100 img-fluid position-relative z-index-10" style="height: 350px !important;" title="" src="data:image/jpg;base64,<?= base64_encode($asaladito->imagen) ?>" alt="">
                                    </picture>
                                    <div class="position-absolute start-0 bottom-0 end-0 z-index-20 p-2">
                                        <button class="btn btn-quick-add"><i class="ri-add-line me-2"></i> Acceder al producto</button>
                                    </div>
                                </div>
                                <div class="card-body px-0">
                                    <a class="text-decoration-none link-cover" href="/product/<?= $asaladito->id ?>"><?= $asaladito->nombre ?></a>
                                    <small class="text-muted d-block"><?= $asaladito->descripcion ?></small>
                                    <p class="mt-2 mb-0 small"><span class="text-danger">$<?= $asaladito->precio ?></span></p>
                                </div>
                            </div>
                            <!--/ Card Product-->
                        </div>
                    <?php endforeach; ?>
                </div>
                <!-- / Products-->

            </div>

            <!-- /Page Content -->
        </section>
        <!-- / Main Section-->

        <!-- Footer -->
        <footer class="border-top py-5 mt-4  ">
            <div class="container-fluid">
                <div class="d-flex justify-content-between align-items-center flex-column flex-lg-row">
                    <div>
                        <ul class="list-unstyled">
                            <li class="d-inline-block me-1"><a class="text-decoration-none text-dark-hover transition-all"
                                                               href="#"><i class="ri-instagram-fill"></i></a></li>
                            <li class="d-inline-block me-1"><a class="text-decoration-none text-dark-hover transition-all"
                                                               href="#"><i class="ri-facebook-fill"></i></a></li>
                            <li class="d-inline-block me-1"><a class="text-decoration-none text-dark-hover transition-all"
                                                               href="#"><i class="ri-twitter-fill"></i></a></li>
                            <li class="d-inline-block me-1"><a class="text-decoration-none text-dark-hover transition-all"
                                                               href="#"><i class="ri-snapchat-fill"></i></a></li>
                        </ul>
                    </div>
                    <div class="d-flex align-items-center justify-content-end flex-column flex-lg-row">
                        <p class="small m-0 text-center text-lg-start">&copy; PESCADORES S.A</p>
                        <ul class="list-unstyled mb-0 ms-lg-4 mt-3 mt-lg-0 d-flex justify-content-end align-items-center">
                            <li class="bg-light p-2 d-flex align-items-center justify-content-center me-2">
                                <i class="pi pi-sm pi-paypal"></i></li>
                            <li class="bg-light p-2 d-flex align-items-center justify-content-center me-2">
                                <i class="pi pi-sm pi-mastercard"></i></li>
                            <li class="bg-light p-2 d-flex align-items-center justify-content-center me-2">
                                <i class="pi pi-sm pi-american-express"></i></li>
                            <li class="bg-light p-2 d-flex align-items-center justify-content-center"><i
                                    class="pi pi-sm pi-visa"></i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>    <!-- / Footer-->

        <!-- Offcanvas Imports-->
        <!-- Filters Offcanvas-->
        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasFilters" aria-labelledby="offcanvasFiltersLabel">
            <div class="offcanvas-header pb-0 d-flex align-items-center">
                <h5 class="offcanvas-title" id="offcanvasFiltersLabel">Category Filters</h5>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <div class="d-flex flex-column justify-content-between w-100 h-100">

                    <!-- Filters-->
                    <div>

                        <!-- Price Filter -->
                        <div class="py-4 widget-filter widget-filter-price border-top">
                            <a class="small text-body text-decoration-none text-secondary-hover transition-all transition-all fs-6 fw-bolder d-block collapse-icon-chevron"
                               data-bs-toggle="collapse" href="#filter-modal-price" role="button" aria-expanded="true"
                               aria-controls="filter-modal-price">
                                Price
                            </a>
                            <div id="filter-modal-price" class="collapse show">
                                <div class="filter-price mt-6"></div>
                                <div class="d-flex justify-content-between align-items-center mt-7">
                                    <div class="input-group mb-0 me-2 border">
                                        <span class="input-group-text bg-transparent fs-7 p-2 text-muted border-0">$</span>
                                        <input type="number" min="00" max="1000" step="1" class="filter-min form-control-sm border flex-grow-1 text-muted border-0">
                                    </div>   
                                    <div class="input-group mb-0 ms-2 border">
                                        <span class="input-group-text bg-transparent fs-7 p-2 text-muted border-0">$</span>
                                        <input type="number" min="00" max="1000" step="1" class="filter-max form-control-sm flex-grow-1 text-muted border-0">
                                    </div>                
                                </div>          </div>
                        </div>
                        <!-- / Price Filter -->

                        <!-- Brands Filter -->
                        <div class="py-4 widget-filter border-top">
                            <a class="small text-body text-decoration-none text-secondary-hover transition-all transition-all fs-6 fw-bolder d-block collapse-icon-chevron"
                               data-bs-toggle="collapse" href="#filter-modal-brands" role="button" aria-expanded="true"
                               aria-controls="filter-modal-brands">
                                Brands
                            </a>
                            <div id="filter-modal-brands" class="collapse show">
                                <div class="input-group my-3 py-1">
                                    <input type="text" class="form-control py-2 filter-search rounded" placeholder="Search"
                                           aria-label="Search">
                                    <span class="input-group-text bg-transparent p-2 position-absolute top-10 end-0 border-0 z-index-20"><i
                                            class="ri-search-2-line text-muted"></i></span>
                                </div>
                                <div class="simplebar-wrapper">
                                    <div class="filter-options" data-pixr-simplebar>
                                        <div class="form-group form-check-custom mb-1">
                                            <input type="checkbox" class="form-check-input" id="filter-brands-modal-0">
                                            <label class="form-check-label fw-normal text-body flex-grow-1 d-flex align-items-center"
                                                   for="filter-brands-modal-0">Adidas  <span
                                                    class="text-muted ms-1 fs-9">(21)</span></label>
                                        </div>                <div class="form-group form-check-custom mb-1">
                                            <input type="checkbox" class="form-check-input" id="filter-brands-modal-1">
                                            <label class="form-check-label fw-normal text-body flex-grow-1 d-flex align-items-center"
                                                   for="filter-brands-modal-1">Asics  <span
                                                    class="text-muted ms-1 fs-9">(13)</span></label>
                                        </div>                <div class="form-group form-check-custom mb-1">
                                            <input type="checkbox" class="form-check-input" id="filter-brands-modal-2">
                                            <label class="form-check-label fw-normal text-body flex-grow-1 d-flex align-items-center"
                                                   for="filter-brands-modal-2">Canterbury  <span
                                                    class="text-muted ms-1 fs-9">(18)</span></label>
                                        </div>                <div class="form-group form-check-custom mb-1">
                                            <input type="checkbox" class="form-check-input" id="filter-brands-modal-3">
                                            <label class="form-check-label fw-normal text-body flex-grow-1 d-flex align-items-center"
                                                   for="filter-brands-modal-3">Converse  <span
                                                    class="text-muted ms-1 fs-9">(25)</span></label>
                                        </div>                <div class="form-group form-check-custom mb-1">
                                            <input type="checkbox" class="form-check-input" id="filter-brands-modal-4">
                                            <label class="form-check-label fw-normal text-body flex-grow-1 d-flex align-items-center"
                                                   for="filter-brands-modal-4">Donnay  <span
                                                    class="text-muted ms-1 fs-9">(11)</span></label>
                                        </div>                <div class="form-group form-check-custom mb-1">
                                            <input type="checkbox" class="form-check-input" id="filter-brands-modal-5">
                                            <label class="form-check-label fw-normal text-body flex-grow-1 d-flex align-items-center"
                                                   for="filter-brands-modal-5">Nike  <span
                                                    class="text-muted ms-1 fs-9">(19)</span></label>
                                        </div>                <div class="form-group form-check-custom mb-1">
                                            <input type="checkbox" class="form-check-input" id="filter-brands-modal-6">
                                            <label class="form-check-label fw-normal text-body flex-grow-1 d-flex align-items-center"
                                                   for="filter-brands-modal-6">Millet  <span
                                                    class="text-muted ms-1 fs-9">(24)</span></label>
                                        </div>                <div class="form-group form-check-custom mb-1">
                                            <input type="checkbox" class="form-check-input" id="filter-brands-modal-7">
                                            <label class="form-check-label fw-normal text-body flex-grow-1 d-flex align-items-center"
                                                   for="filter-brands-modal-7">Puma  <span
                                                    class="text-muted ms-1 fs-9">(11)</span></label>
                                        </div>                <div class="form-group form-check-custom mb-1">
                                            <input type="checkbox" class="form-check-input" id="filter-brands-modal-8">
                                            <label class="form-check-label fw-normal text-body flex-grow-1 d-flex align-items-center"
                                                   for="filter-brands-modal-8">Reebok  <span
                                                    class="text-muted ms-1 fs-9">(19)</span></label>
                                        </div>                <div class="form-group form-check-custom mb-1">
                                            <input type="checkbox" class="form-check-input" id="filter-brands-modal-9">
                                            <label class="form-check-label fw-normal text-body flex-grow-1 d-flex align-items-center"
                                                   for="filter-brands-modal-9">Under Armour  <span
                                                    class="text-muted ms-1 fs-9">(24)</span></label>
                                        </div>              </div>
                                </div>
                            </div>
                        </div>
                        <!-- / Brands Filter -->

                        <!-- Sizes Filter -->
                        <div class="py-4 widget-filter border-top">
                            <a class="small text-body text-decoration-none text-secondary-hover transition-all transition-all fs-6 fw-bolder d-block collapse-icon-chevron"
                               data-bs-toggle="collapse" href="#filter-modal-sizes" role="button" aria-expanded="true"
                               aria-controls="filter-modal-sizes">
                                Sizes
                            </a>
                            <div id="filter-modal-sizes" class="collapse show">
                                <div class="filter-options mt-3">
                                    <div class="form-group d-inline-block mr-2 mb-2 form-check-bg form-check-custom">
                                        <input type="checkbox" class="form-check-bg-input" id="filter-sizes-modal-0">
                                        <label class="form-check-label fw-normal" for="filter-sizes-modal-0">6.5</label>
                                    </div>              <div class="form-group d-inline-block mr-2 mb-2 form-check-bg form-check-custom">
                                        <input type="checkbox" class="form-check-bg-input" id="filter-sizes-modal-1">
                                        <label class="form-check-label fw-normal" for="filter-sizes-modal-1">7</label>
                                    </div>              <div class="form-group d-inline-block mr-2 mb-2 form-check-bg form-check-custom">
                                        <input type="checkbox" class="form-check-bg-input" id="filter-sizes-modal-2">
                                        <label class="form-check-label fw-normal" for="filter-sizes-modal-2">7.5</label>
                                    </div>              <div class="form-group d-inline-block mr-2 mb-2 form-check-bg form-check-custom">
                                        <input type="checkbox" class="form-check-bg-input" id="filter-sizes-modal-3">
                                        <label class="form-check-label fw-normal" for="filter-sizes-modal-3">8</label>
                                    </div>              <div class="form-group d-inline-block mr-2 mb-2 form-check-bg form-check-custom">
                                        <input type="checkbox" class="form-check-bg-input" id="filter-sizes-modal-4">
                                        <label class="form-check-label fw-normal" for="filter-sizes-modal-4">8.5</label>
                                    </div>              <div class="form-group d-inline-block mr-2 mb-2 form-check-bg form-check-custom">
                                        <input type="checkbox" class="form-check-bg-input" id="filter-sizes-modal-5">
                                        <label class="form-check-label fw-normal" for="filter-sizes-modal-5">9</label>
                                    </div>              <div class="form-group d-inline-block mr-2 mb-2 form-check-bg form-check-custom">
                                        <input type="checkbox" class="form-check-bg-input" id="filter-sizes-modal-6">
                                        <label class="form-check-label fw-normal" for="filter-sizes-modal-6">9.5</label>
                                    </div>              <div class="form-group d-inline-block mr-2 mb-2 form-check-bg form-check-custom">
                                        <input type="checkbox" class="form-check-bg-input" id="filter-sizes-modal-7">
                                        <label class="form-check-label fw-normal" for="filter-sizes-modal-7">10</label>
                                    </div>              <div class="form-group d-inline-block mr-2 mb-2 form-check-bg form-check-custom">
                                        <input type="checkbox" class="form-check-bg-input" id="filter-sizes-modal-8">
                                        <label class="form-check-label fw-normal" for="filter-sizes-modal-8">10.5</label>
                                    </div>              <div class="form-group d-inline-block mr-2 mb-2 form-check-bg form-check-custom">
                                        <input type="checkbox" class="form-check-bg-input" id="filter-sizes-modal-9">
                                        <label class="form-check-label fw-normal" for="filter-sizes-modal-9">11</label>
                                    </div>              <div class="form-group d-inline-block mr-2 mb-2 form-check-bg form-check-custom">
                                        <input type="checkbox" class="form-check-bg-input" id="filter-sizes-modal-10">
                                        <label class="form-check-label fw-normal" for="filter-sizes-modal-10">11.5</label>
                                    </div>            </div>
                            </div>
                        </div>
                        <!-- / Sizes Filter -->

                        <!-- Colour Filter -->
                        <div class="py-4 widget-filter border-top">
                            <a class="small text-body text-decoration-none text-secondary-hover transition-all transition-all fs-6 fw-bolder d-block collapse-icon-chevron"
                               data-bs-toggle="collapse" href="#filter-modal-colour" role="button" aria-expanded="true"
                               aria-controls="filter-modal-colour">
                                Colour
                            </a>
                            <div id="filter-modal-colour" class="collapse show">
                                <div class="filter-options mt-3">
                                    <div class="form-group d-inline-block mr-1 mb-1 form-check-solid-bg-checkmark form-check-custom form-check-primary">
                                        <input type="checkbox" class="form-check-color-input" id="filter-colours-modal-0">
                                        <label class="form-check-label" for="filter-colours-modal-0"></label>
                                    </div>              <div class="form-group d-inline-block mr-1 mb-1 form-check-solid-bg-checkmark form-check-custom form-check-success">
                                        <input type="checkbox" class="form-check-color-input" id="filter-colours-modal-1">
                                        <label class="form-check-label" for="filter-colours-modal-1"></label>
                                    </div>              <div class="form-group d-inline-block mr-1 mb-1 form-check-solid-bg-checkmark form-check-custom form-check-danger">
                                        <input type="checkbox" class="form-check-color-input" id="filter-colours-modal-2">
                                        <label class="form-check-label" for="filter-colours-modal-2"></label>
                                    </div>              <div class="form-group d-inline-block mr-1 mb-1 form-check-solid-bg-checkmark form-check-custom form-check-info">
                                        <input type="checkbox" class="form-check-color-input" id="filter-colours-modal-3">
                                        <label class="form-check-label" for="filter-colours-modal-3"></label>
                                    </div>              <div class="form-group d-inline-block mr-1 mb-1 form-check-solid-bg-checkmark form-check-custom form-check-warning">
                                        <input type="checkbox" class="form-check-color-input" id="filter-colours-modal-4">
                                        <label class="form-check-label" for="filter-colours-modal-4"></label>
                                    </div>              <div class="form-group d-inline-block mr-1 mb-1 form-check-solid-bg-checkmark form-check-custom form-check-dark">
                                        <input type="checkbox" class="form-check-color-input" id="filter-colours-modal-5">
                                        <label class="form-check-label" for="filter-colours-modal-5"></label>
                                    </div>              <div class="form-group d-inline-block mr-1 mb-1 form-check-solid-bg-checkmark form-check-custom form-check-secondary">
                                        <input type="checkbox" class="form-check-color-input" id="filter-colours-modal-6">
                                        <label class="form-check-label" for="filter-colours-modal-6"></label>
                                    </div>            </div>
                            </div>
                        </div>
                        <!-- / Colour Filter -->
                    </div>
                    <!-- / Filters-->

                    <!-- Filter Button-->
                    <div class="border-top pt-3">
                        <a href="#" class="btn btn-dark mt-2 d-block hover-lift-sm hover-boxshadow" data-bs-dismiss="offcanvas" aria-label="Close">Done</a>
                    </div>
                    <!-- /Filter Button-->
                </div>
            </div>
        </div>
        <!-- Theme JS -->
        <!-- Vendor JS -->
        <script src="./assets/js/vendor.bundle.js"></script>

        <!-- Theme JS -->
        <script src="./assets/js/theme.bundle.js"></script>
    </body>

</html>
