<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::home');
$routes->get('/home', 'PecesController::home');
$routes->get('/aguaDulce', 'PecesController::adulce');
$routes->get('/aguaSalada', 'PecesController::asalada');
$routes->get('/register', 'PecesController::registro');
$routes->get('/forgotten-password', 'PecesController::colvidar');
$routes->get('/login', 'PecesController::cuenta');
$routes->get('/cart', 'PecesController::carrito');
$routes->get('/checkout', 'PecesController::compra');
$routes->get('/checkout-shipping', 'PecesController::metodoenvio');
$routes->get('/checkout-payment', 'PecesController::formapago');
$routes->get('/product/(:num)', 'PecesController::productosASalada/$1');
$routes->get('/productDulce/(:num)', 'PecesController::productosADulce/$1');


$routes->get('/anyadirProd', 'PecesController::anyadirProd');
$routes->post('/anyadirProd', 'PecesController::anyadirProd');
$routes->get('/eliminar/(:num)', 'PecesController::eliminar/$1');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
