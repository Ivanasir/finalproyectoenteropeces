<?php

namespace app\Controllers;



class Ciclo extends BaseController

{
     
    public function muestraCiclo($grupo)
    {
        $data['title'] = 'Listado Ciclos';
        $alumnoModel = new \App\Models\GrupoModel();
        
        $data ['resultado' ] = $alumnoModel
	->select ('*')
	->from ('alumnos as al')
	->join('matricula as matr','matr.NIA=al.NIA','left') 
	->where (['matr.grupo'=>$grupo])
        //->distinct('al.email')
	->findAll();
     

        
        return view('alumno/lista',$data);
            
        //echo '<pre>';
        //    print_r($data);
        //echo '</pre>';

    }
}

