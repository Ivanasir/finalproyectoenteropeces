<?= $this->extend('templates/default') ?>

<?= $this->section('head_title') ?>
<?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('title') ?> 
<div class="bg-dark text-light p-3">
<div class="float-right">
<?php if ($ionAuth->loggedIn()): ?>
    <?php $user = $ionAuth->user()->row(); ?>
    <span class="bi bi-person-circle text-light mr-2"></span><?= $user->first_name . ' ' . $user->last_name ?>
    logout: <a href="<?=site_url('auth/logout')?>"><span class="bi bi-door-closed mr-2 text-warning"></span></a>
<?php else: ?>
    login: <a href="<?=site_url('auth/login')?>  "><span class="bi bi-door-open mr-2 text-warning"></span></a>
<?php endif; ?>
</div>    
<div>
    <span class="h1"><?= $title ?></span>
</div>
</div>    
<?= $this->endSection() ?>

<?= $this->section('content') ?>

<?php foreach ($posts as $post): ?>
    <div class="row">
        <div class="col-12 bg-secondary text-white">    
            <h2><?= $post->titulo ?></h2>
        </div>
        <div class="col-12 bg-info text-white">
            <span class="bi bi-person-lines-fill mr-2"></span>Autor: <?= $post->autor ?>
            <span class="bi bi-calendar3 ml-5 mr-2"></span><?= date('d M Y H:i', strtotime($post->creado_at)); ?>
            <span class="bi bi-chat-left-text ml-5 mr-2"></span><?= $post->num_comentarios ?> comentarios
        </div>
        <div class="col-12 bg-light pt-3 pb-3">
            <img src="<?= base_url('assets/images/' . $post->id . '.jpg') ?>"  class="float-lg-right">
            <?= substr(str_replace("\n", "<br>", $post->contenido), 0, 500) ?> ...
            <a href="<?= Base_url('comentarios/'.$post->id)?> method="post"  class="btn btn-dark">Leer mas</a>
           
        </div>
         <a href="" method="post"  class="btn btn-dark">Añadir Post</a>
    </div>    
<?php endforeach; ?>        

<?= $this->endSection() ?>
