<!doctype html>

<html lang="en">
    <head>
        <style>

           html,
body {
  height: 100%;
}

body.login {
  display: -ms-flexbox;
  display: -webkit-box;
  display: flex;
  -ms-flex-align: center;
  -ms-flex-pack: center;
  -webkit-box-align: center;
  align-items: center;
  -webkit-box-pack: center;
  justify-content: center;
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #f5f5f5;
}

.form-signin {
  width: 100%;
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
        </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Signin Template for Ion Auth</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url('../assets/css/style.css'); ?>">
  </head>
 
  <body class="text-center login" >
    <div class='form-signin'>
      <h1><?php echo lang('Auth.login_heading');?></h1>
      <p><?php echo lang('Auth.login_subheading');?></p>
      <div id="infoMessage"><?php echo $message;?></div>
      <?php echo form_open('auth/login');?>
        <p>
          <?php echo form_label(lang('Auth.login_identity_label'), 'identity');?>
          <?php echo form_input($identity,set_value('identity'),['class'=>'form-control', 'required'=>'required', 'autofocus'=>'autofocus']);?>
        </p>
        <p>
          <?php echo form_label(lang('Auth.login_password_label'), 'password');?>
          <?php echo form_input($password, 'password', ['class'=>'form-control', 'required'=>'required']);?>
        </p>
        <p>
          <?php echo form_label(lang('Auth.login_remember_label'), 'remember');?>
          <?php echo form_checkbox('remember', '1', false, ['class'=>'form-control', 'id'=>'remember']);?>
        </p>
        <p><?php echo form_submit('submit', lang('Auth.login_submit_btn'),['class'=>'btn btn-lg btn-primary btn-block']);?></p>
      <?php echo form_close();?>
      <p><a href="forgot_password"><?php echo lang('Auth.login_forgot_password');?></a></p>
      </div>
  </body>
</html>


