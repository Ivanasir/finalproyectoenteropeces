<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <form action="<?= site_url('usuario/boton')?>" method="post">
        <div class="form-group">
            <?= form_label('id:', 'id', ['class'=>'col-2'])?>
            <?= form_input('id',set_value('id',''),['class'=>'form_control col-9', 'id'=>'id']) ?>
        </div>
        <div class="form-group">
            <?= form_label('titulo:', 'titulo', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('titulo',set_value('titulo',''),['class'=>'form_control col-9', 'id'=>'titulo']) ?>
        </div>
        <div class="form-group">
            <?= form_label('contenido', 'contenido', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('contenido',set_value('contenido',''),['class'=>'form_control col-9', 'id'=>'contenido']) ?>
        </div>
        <div class="form-group">
            <?= form_label('creado_at:', 'creado_at', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('creado_at',set_value('creado_at',''),['class'=>'form_control col-9', 'id'=>'creado_at']) ?>
        </div>
        <div class="form-group">
            <?= form_label('USUARIO:', 'usuario', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('usuario',set_value('usuario',''),['class'=>'form_control col-9', 'id'=>'usuario']) ?>
        </div>
        
        <input type="submit" name="enviar" value="Enviar" />
    </form>
<?= $this->endSection() ?>


