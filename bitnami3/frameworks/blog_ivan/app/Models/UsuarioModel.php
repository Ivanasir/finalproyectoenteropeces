<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;

/**
 * Description of AlumnoModel
 *
 * @author jose
 */
class UsuarioModel extends Model{ //Ahora tengo un modelo, sino sería un clase normal sin superpoderes.
    protected $table = 'post';
    protected $primaryKey = 'id';
    protected $returnType = 'object'; //porque me gusta
    //hemos de decir que campos son modificables.
    protected $allowedFields = ['id','titulo','contenido','creado_at','usuario'];
    protected $validationRules = [
        'id'       => 'required|numeric|min_length[8]|is_unique[comentarios.id]',
        'titulo'    => 'required',
        'contenido' => 'trim|required',
        'creado_at'     => 'datetime',
        'usuario'       => 'is_unique[usuarios.id]',
    ];
}
