<?= $this->extend('templates/default') ?>

//Disponemos de 5 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <form action="<?= site_url('insertar/creaGrupo')?>" method="post">

        <div class="form-group">
            <?= form_label('Codigo:', 'codigo', ['class'=>'col-2'])?>
            <?= form_input('codigo',set_value('codigo',''),['class'=>'form_control col-9', 'id'=>'id']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Grupo:', 'nombre', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nombre',set_value('nombre',''),['class'=>'form_control col-9', 'id'=>'nombre']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Grupo:', 'nombre', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nombre',set_value('nombre',''),['class'=>'form_control col-9', 'id'=>'descripcion']) ?>
        </div>
        <div class="form-group">
        <?= form_label('Grupo:', 'nombre', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nombre',set_value('nombre',''),['class'=>'form_control col-9', 'id'=>'localidad']) ?>
        </div>
         <div class="form-group">
        <?= form_label('Grupo:', 'nombre', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nombre',set_value('nombre',''),['class'=>'form_control col-9', 'id'=>'cp']) ?>
        </div>

        <input class="btn btn-primary" type="submit" name="enviar" value="Enviar" />
    </form>
<?= $this->endSection() ?>

