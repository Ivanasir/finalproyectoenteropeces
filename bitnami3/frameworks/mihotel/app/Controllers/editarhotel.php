<?php


namespace App\Controllers;
use App\Models\Hotelmodel; 
 


class editarhotel extends BaseController {
    
    public function hoteledit($id){
        helper('form');
        $gruposModel = new GrupoModel();
        $data['title'] = 'Modificar Hotel';
        $data['grupo'] = $gruposModel->find($id);
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        return view('grupo/editarhotel',$data);
    }
    
    public function edithotel($id){
        
        helper('form');
        $gruposModel = new Hotelmodel();
        $data['title'] = 'Modificar Hotel';
        $data['grupo'] = $gruposModel->find($id);
        $grupo = $this->request->getPost();
        unset($grupo['boton']);
        /*
        echo '<pre>';
             print_r($grupo);
        echo '</pre>';
        */
        $grupoModel = new GrupoModel();
        $grupoModel->update($id,$grupo);
        
        if ($grupoModel->update($id,$grupo)=== false){
            $data['errores'] = $grupoModel->errors();
            $data['title'] = 'Modificar Hotel';
            return view('grupo/editarhotel',$data);
        }
        
        return redirect('grupo/listadohoteles');
    }

}