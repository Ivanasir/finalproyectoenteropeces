<?php

namespace App\Models;
use CodeIgniter\Model;

class Hotelmodel extends Model{ 
    protected $table = 'hoteles';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $allowedFields = ['id', 'cp', 'nombre'];
    
    protected $validationRules = [
        'id'    => 'required',
        'nombre'    => 'required',
    ];
    
     
}